package ru.tsc.panteleev.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.panteleev.tm.api.endpoint.*;
import ru.tsc.panteleev.tm.api.service.IPropertyService;

@Configuration
@ComponentScan("ru.tsc.panteleev.tm")
public class ClientConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public IDomainEndpoint domainEndpoint() {
        return IDomainEndpoint.newInstance(propertyService.getHost(),propertyService.getPort());
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        return IProjectEndpoint.newInstance(propertyService.getHost(),propertyService.getPort());
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        return ITaskEndpoint.newInstance(propertyService.getHost(),propertyService.getPort());
    }

    @Bean
    @NotNull
    public IProjectTaskEndpoint projectTaskEndpoint() {
        return IProjectTaskEndpoint.newInstance(propertyService.getHost(),propertyService.getPort());
    }

    @Bean
    @NotNull
    protected IUserEndpoint userEndpoint() {
        return IUserEndpoint.newInstance(propertyService.getHost(),propertyService.getPort());
    }

    @Bean
    @NotNull
    protected IAuthEndpoint authEndpoint() {
        return IAuthEndpoint.newInstance(propertyService.getHost(),propertyService.getPort());
    }

    @Bean
    @NotNull
    public ISystemEndpoint systemEndpoint() {
        return ISystemEndpoint.newInstance(propertyService.getHost(),propertyService.getPort());
    }

}
