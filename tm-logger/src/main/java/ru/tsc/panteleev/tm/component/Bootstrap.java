package ru.tsc.panteleev.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.api.IReceiverService;
import ru.tsc.panteleev.tm.listener.LogListener;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private LogListener logListener;

    public void run() {
        receiverService.receive(logListener);
    }

}
