package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        for (int i = 1; i < 11; i++) {
            write(new Task("Task " + i, UUID.randomUUID().toString()));
        }

    }

    public void create() {
        write(new Task(("Task " + System.currentTimeMillis() / 1000), UUID.randomUUID().toString()));
    }

    public void write(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

}
