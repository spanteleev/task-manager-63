package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        for (int i = 1; i < 11; i++) {
            write(new Project("Project " + i, UUID.randomUUID().toString()));
        }

    }

    public void create() {
        write(new Project(("Project " + System.currentTimeMillis() / 1000), UUID.randomUUID().toString()));
    }

    public void write(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
