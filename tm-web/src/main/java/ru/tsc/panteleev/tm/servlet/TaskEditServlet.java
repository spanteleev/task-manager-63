package ru.tsc.panteleev.tm.servlet;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.repository.ProjectRepository;
import ru.tsc.panteleev.tm.repository.TaskRepository;
import ru.tsc.panteleev.tm.util.DateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String projectId = req.getParameter("projectId");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String statusValue = req.getParameter("status");
        @NotNull final Status status = Status.valueOf(statusValue);
        @NotNull final String dateBeginValue = req.getParameter("dateBegin");
        @NotNull final String dateEndValue = req.getParameter("dateEnd");
        Task task = new Task(
                id, name, description, status,
                DateUtil.toDate(dateBeginValue), DateUtil.toDate(dateEndValue),
                projectId.isEmpty() ? null : projectId
        );
        TaskRepository.getInstance().write(task);
        resp.sendRedirect("/tasks");
    }

}

