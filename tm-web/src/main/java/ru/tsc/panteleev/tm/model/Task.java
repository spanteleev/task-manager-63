package ru.tsc.panteleev.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected Date dateBegin;

    @Nullable
    protected Date dateEnd;

    @Nullable
    protected String projectId = null;

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }
}
