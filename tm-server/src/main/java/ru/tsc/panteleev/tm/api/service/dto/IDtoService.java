package ru.tsc.panteleev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDto;

import java.util.Collection;
import java.util.List;

public interface IDtoService<M extends AbstractModelDto> {

    void set(@NotNull Collection<M> models);

    @Nullable
    List<M> findAll();

}
