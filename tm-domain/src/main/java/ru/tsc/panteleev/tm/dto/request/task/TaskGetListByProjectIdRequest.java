package ru.tsc.panteleev.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskGetListByProjectIdRequest(@Nullable String token, @Nullable String projectId) {
        super(token);
        this.projectId = projectId;
    }

}
